cmolpro
=======

A command-line interface to the molpro-project library, allowing the manipulation of [Molpro](https://www.molpro.net) project bundles.  A bundle is a directory containing at least Molpro input, geometry and output files, plus a registry of properties.

The principal operations that can be carried out are

new
: Make a completely new project bundle

edit
: Edit the Molpro input file

browse
: Browse the Molpro output file

run
: Launch a job either on the local machine or on a preconfigured backend

status
: Report the status of the job launched by run

kill
: Kill the job launched by run

erase
: Erase the project

copy
: Make a copy of the project bundle

move
:  Move the project bundle

import
: Copy files into the project bundle

export
: Copy files out of the project bundle

Backends are defined in a configuration file located at ~/.molpro/backends.xml and/or /usr/local/etc/molpro/backends.xml. Example:

```xml
<backends>
  <!-- informal immediate launching of Molpro on a neighbouring workstation -->
    <backend name="linux" host="user@host" cache="/tmp/molpro-backend" run_command="molpro"/>
    <backend name="linux2" host="linux2" cache="/tmp/peter/molpro-backend" run_command="myMolpro/bin/molpro"/>
  <!-- an example of a Slurm system, with qmolpro a wrapper that constructs a molpro job script,
       and submits with srun -->
    <backend name="slurmcluster"
             host="someone@slurmcluster.somewhere.edu"
             cache="/scratch/someone/molpro-project"
             run_command="/home/someone/software/molpro/release/bin/qmolpro"
             run_jobnumber="Submitted batch job *([0-9]+)"
             kill_command="scancel"
             status_command="squeue -j"
             status_running=" (CF|CG|R|ST|S) *[0-9]" status_waiting=" (PD|SE) *[0-9]"
    />
</backends>
```
For more information,
```sh
cmolpro --help
```
