#include <gtest/gtest.h>
#include <gmock/gmock.h>

#include "molpro-project.h"
#include "molpro-backend.h"
#include <map>
#include <list>
#include <unistd.h>
#include <libgen.h>
#include <boost/filesystem.hpp>

namespace fs = boost::filesystem;

class savestate {
  std::string rf;
 public:
  savestate() {
    rf = molpro::expand_path(molpro::recentProjectsFile);
    if (!fs::exists(rf)) rf.clear();
    if (!rf.empty()) {
      fs::rename(rf, rf + ".save");
//      std::cerr << "savestate saves " << rf << std::endl;
    }
  }
  ~savestate() {
    if (!rf.empty()) {
//      std::cerr << "savestate restores " << rf << std::endl;
      fs::rename(rf + ".save", rf);
    }
  }

};
TEST(project, filename) {
  std::string slash{boost::filesystem::path::preferred_separator};
  char* e = getenv(
#ifdef _WIN32
      "TEMP"
#else
      "TMPDIR"
#endif
  );
  std::string tmpdir{e == nullptr ? "/tmp" : e};
  std::map<std::string, std::string> names;
//  system((std::string{"ls -la "}+tmpdir).c_str());
  names["$TMPDIR/tmp.molpro/"] = tmpdir + slash + "tmp.molpro";
  names["$TMPDIR/tmp"] = tmpdir + slash + "tmp.molpro";
  for (const auto& n : names)
    ASSERT_EQ(molpro::project(n.first, nullptr, true, false).filename(), n.second);
}

TEST(project, expand_path) {
  std::string slash{boost::filesystem::path::preferred_separator};
  std::string cwd{boost::filesystem::current_path().string()};
  std::string home{getenv(
#ifdef _WIN32
      "USERPROFILE"
#else
      "HOME"
#endif
  )};
  std::map<std::string, std::string> names;
  names["/x/y/z"] = slash + "x" + slash + "y" + slash + "z";
  names["\\x/y\\z"] = slash + "x" + slash + "y" + slash + "z";
  names["/x.ext"] = slash + "x.ext";
  names["x/y/z"] = cwd + slash + "x" + slash + "y" + slash + "z";
  names["~/x"] = home + slash + "x";
  names["$HOME/x"] = home + slash + "x";
  names["${HOME}/x"] = home + slash + "x";
  for (const auto& n : names)
    ASSERT_EQ(molpro::expand_path(n.first), n.second);
  names.clear();
  names["/x"] = slash + "x.ext";
  names["x"] = cwd + slash + "x.ext";
  names["/x.ext1"] = slash + "x.ext1";
  names["x.ext1"] = cwd + slash + "x.ext1";
  for (const auto& n : names)
    ASSERT_EQ(molpro::expand_path(n.first, "ext"), n.second);
}

TEST(project, construction) {
  std::string name("molpro-project-test");
  for (const auto& dirs : std::vector<std::string>{"$TMPDIR", "$HOME", "${HOME}", "~"}) {
    std::string filename(dirs + "/" + name + ".molpro");
    molpro::project::erase(filename); // remove any previous contents
    molpro::project x(filename, nullptr, true);
    ASSERT_EQ(x.name(), name);
  }
}

TEST(project, move) {
  std::string name("molpro-project-test");
  std::string filename("$TMPDIR/" + name + ".molpro");
  molpro::project::erase(filename); // remove any previous contents
  molpro::project x2(filename, nullptr, true);
  ASSERT_EQ (x2.name(), name);
  auto name2 = name + "2";
  std::string filename2("$TMPDIR/" + name2 + ".molpro");
  molpro::project::erase(filename2); // remove any previous contents
  x2.move(filename2);
  ASSERT_EQ(x2.name(), name2);
  ASSERT_FALSE(fs::exists(fs::path(filename)));
  ASSERT_TRUE(fs::exists(fs::path(x2.filename())));
}

TEST(project, erase) {
  std::string filename("$TMPDIR/molpro-project-test.molpro");
  molpro::project::erase(filename); // remove any previous contents
  molpro::project x(filename);
  filename = x.filename();
  ASSERT_TRUE(fs::exists(fs::path(filename)));
  ASSERT_TRUE(fs::exists(fs::path(filename + "/Info.plist")));
  ASSERT_TRUE(fs::is_directory(fs::path(filename)));
  x.erase();
  ASSERT_EQ(x.filename(), "");
  ASSERT_FALSE(fs::exists(fs::path(filename)));
}

TEST(project, import) {
  std::string filename("$TMPDIR/molpro-project-test.molpro");
  molpro::project::erase(filename); // remove any previous contents
  molpro::project x(filename);
  filename = x.filename();
  auto importfile=molpro::expand_path("$TMPDIR/molpro-project-test.import");
  boost::filesystem::ofstream ofs{importfile};
  ofs << "Hello"<<std::endl;
  x.Import(importfile);
  x.Import(importfile,true);
}

TEST(project, clean) {
  std::string filename("$TMPDIR/molpro-project-test.molpro");
  molpro::project::erase(filename); // remove any previous contents
  molpro::project x(filename);
  filename = x.filename();
  ASSERT_FALSE(fs::exists(fs::path(filename) / fs::path(filename + ".out")));
  { fs::ofstream s(fs::path(filename) / fs::path(x.name() + ".out")); }
  ASSERT_TRUE(fs::exists(fs::path(filename) / fs::path(x.name() + ".out")));
  x.clean(true, true);
  ASSERT_FALSE(fs::exists(fs::path(filename) / fs::path(x.name() + ".out")));
  ASSERT_FALSE(fs::is_directory(fs::path(filename) / fs::path(x.name() + ".d")));
  fs::create_directory(fs::path(filename) / fs::path(x.name() + ".d"));
  ASSERT_TRUE(fs::is_directory(fs::path(filename) / fs::path(x.name() + ".d")));
  x.clean(true);
  ASSERT_FALSE(fs::is_directory(fs::path(filename) / fs::path(x.name() + ".d")));
}

TEST(project, properties) {
  molpro::project::erase("$TMPDIR/try.molpro"); // remove any previous contents
  molpro::project x("$TMPDIR/try.molpro", nullptr, true);
  x.property_rewind();
  std::string key;
  int ninitial;
  for (ninitial = 0; (key = x.property_next()) != ""; ++ninitial);
  std::map<std::string, std::string> data;
  data["first key"] = "first value";
  data["second key"] = "second value";
  data["third key"] = "third value";
  for (const auto& keyval : data)
    x.property_set(keyval.first, keyval.second);
  for (const auto& keyval : data) {
//    std::cout << "key "<<keyval.first<<" expect value: "<<keyval.second<<" actual value: "<<x.property_get(keyval.first)<<std::endl;
    ASSERT_EQ(x.property_get(keyval.first), keyval.second);
  }
  x.property_rewind();
  int n;
  for (n = 0; (key = x.property_next()) != ""; ++n) {
//    std::cout << "key "<<key<<std::endl;
    if (data.count("key") != 0)
      ASSERT_EQ(x.property_get(key), data[key]);
  }
//  std::cout << "data.size() "<<data.size()<<std::endl;
  ASSERT_EQ(n, data.size() + ninitial);
  x.property_rewind();
  for (x.property_rewind(); (key = x.property_next()) != ""; x.property_rewind()) {
//    system(("echo start deletion loop key="+key+"; cat "+x.filename()+"/Info.plist").c_str());
    x.property_delete(key);
//    system(("echo end deletion loop key="+key+"; cat "+x.filename()+"/Info.plist").c_str());
  }
  x.property_rewind();
  ASSERT_EQ(x.property_next(), "");

  ASSERT_EQ(x.property_get("vacuous"), "");
  x.property_set("empty", "");
  ASSERT_EQ(x.property_get("empty"), "");
}

TEST(project, recent_files) {
  auto rf = molpro::expand_path(molpro::recentProjectsFile);
  auto oldfile = fs::exists(rf);
  if (oldfile)
    fs::rename(rf, rf + ".save");
  std::list<molpro::project> p;
  for (size_t i = 0; i < 10; i++) {
    molpro::project::erase("$TMPDIR/p" + std::to_string(i)); // remove any previous contents
    p.emplace_back("$TMPDIR/p" + std::to_string(i));
  }
  size_t i = p.size();
  for (const auto& pp : p)
    ASSERT_EQ(molpro::project::recent(i--), pp.filename());
  i = p.size();
  for (const auto& pp : p)
    ASSERT_EQ(molpro::project::recent_find(pp.filename()), i--);
  p.back().erase();
  p.pop_back();
//  system(("cat "+rf).c_str());
  i = p.size();
  for (const auto& pp : p)
    ASSERT_EQ(molpro::project::recent(i--), pp.filename());
  i = p.size();
  for (const auto& pp : p)
    ASSERT_EQ(molpro::project::recent(i--), pp.filename());
  for (auto& pp : p)
    pp.erase();
  for (const auto& pp : p)
    ASSERT_EQ(molpro::project::recent_find(pp.filename()), 0);
  if (oldfile)
    fs::rename(rf + ".save", rf);
}

TEST(project, c_binding) {
  savestate x;
  char projectname[] = "$TMPDIR/test-molpro-project/cproject";
  char projectname2[] = "$TMPDIR/test-molpro-project/cproject2";
  molpro_project_erase(projectname);
  molpro_project_erase(projectname2);
  char key[] = "testkey";
  char value[] = "testvalue";
  char value2[] = "testvalue2";
  molpro_project_property_set(projectname, key, value);
  ASSERT_EQ(std::string{value}, std::string{molpro_project_property_get(projectname, key)});
  molpro_project_property_set(projectname, key, value2);
  ASSERT_EQ(std::string{value2}, std::string{molpro_project_property_get(projectname, key)});
  molpro_project_property_delete(projectname, key);
  ASSERT_EQ(std::string{}, std::string{molpro_project_property_get(projectname, key)});
  ASSERT_EQ(std::string{}, std::string{molpro_project_property_get(projectname, "unknown key")});
  molpro_project_property_set(projectname, key, value);
  molpro_project_copy(projectname, projectname2, 0);
  ASSERT_EQ(std::string{value}, std::string{molpro_project_property_get(projectname, key)});
  ASSERT_EQ(std::string{value}, std::string{molpro_project_property_get(projectname2, key)});
  molpro_project_erase(projectname2);
  ASSERT_EQ(molpro_project_move(projectname, projectname2), 1);
  ASSERT_EQ(std::string{}, std::string{molpro_project_property_get(projectname, key)});
  ASSERT_EQ(std::string{value}, std::string{molpro_project_property_get(projectname2, key)});
  const auto cppbe = molpro::global_backends();
  char** backends = molpro_global_backends();
  int i;
  for (i = 0; backends[i]; i++) {
//    fprintf(stderr,"backend %ld %s\n",backends[i],backends[i]);
    ASSERT_NE(std::find(cppbe.begin(), cppbe.end(), std::string{backends[i]}), cppbe.end());
//    std::cerr << *std::find(cppbe.begin(),cppbe.end(),std::string{backends[i]})<<std::endl;
    free(backends[i]);
  }
  free(backends);
//  molpro_project_erase(projectname);
//  molpro_project_erase(projectname2);
  rmdir(dirname(molpro_expand_path(projectname, "")));
}

TEST(project, project_hash) {
  molpro::project::erase("$TMPDIR/try.molpro"); // remove any previous contents
  molpro::project x("$TMPDIR/try.molpro", nullptr, true);
  auto xph = x.project_hash();
  molpro::project::erase("$TMPDIR/try2.molpro"); // remove any previous contents
  ASSERT_TRUE(x.copy("$TMPDIR/try2.molpro"));
  molpro::project x2("$TMPDIR/try2.molpro", nullptr, true, false);
  ASSERT_NE(xph, x2.project_hash());
  molpro::project::erase("$TMPDIR/try3.molpro"); // remove any previous contents
  x.move("$TMPDIR/try3.molpro");
  ASSERT_EQ(xph, x.project_hash());
}

TEST(project, input_hash) {
  molpro::project::erase("$TMPDIR/try.molpro"); // remove any previous contents
  molpro::project x("$TMPDIR/try.molpro", nullptr, true);
  {
    std::ofstream ss(x.filename("inp"));
    ss << "one\ngeometry=try.xyz\ntwo" << std::endl;
  }
  {
    std::ofstream ss(x.filename("xyz"));
    ss << "1\nThe xyz file\nHe 0 0 0" << std::endl;
  }
  auto xph = x.input_hash();
  molpro::project::erase("$TMPDIR/try2.molpro"); // remove any previous contents
  ASSERT_TRUE(x.copy("$TMPDIR/try2.molpro"));
  molpro::project x2("$TMPDIR/try2.molpro", nullptr, true, false);
  ASSERT_EQ(xph, x2.input_hash());
  molpro::project::erase("$TMPDIR/try3.molpro"); // remove any previous contents
  x.move("$TMPDIR/try3.molpro");
  ASSERT_EQ(xph, x.input_hash());
}