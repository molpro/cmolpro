#include "molpro-backend.h"
#include <iostream>
#include <boost/filesystem.hpp>
#include <map>
#include <regex>
#include <sstream>
#include <pugixml.hpp>
namespace fs = boost::filesystem;

std::map<std::string, molpro::backend> g_backends;
std::string molpro::backend::default_name = "local";

static void g_backends_initialise() {
  molpro::global_backend_add(molpro::backend(molpro::backend::default_name));
  molpro::global_backend_add("/usr/local/etc/molpro/backends.xml", true);
  molpro::global_backend_add("~/.molpro/backends.xml", true);
}

molpro::backend& molpro::global_backend(std::string name) {
  if (g_backends.empty())
    g_backends_initialise();
  if (g_backends.count(name) == 0) {
    throw std::out_of_range("'" + name + "' not found in global backend registry");
  }
  return g_backends.at(name);
}

void molpro::global_backend_add(molpro::backend backend) {
  g_backends.emplace(backend.name, backend);
}

std::vector<std::string> molpro::global_backends() {
  std::vector<std::string> result;
  if (g_backends.empty())
    g_backends_initialise();
  for (const auto& be : g_backends)
    result.push_back(be.first);
  return result;
}

inline std::string getattribute(pugi::xpath_node node, std::string name) {
   return node.node().attribute(name.c_str()).value();
}
void molpro::global_backend_add(std::string config_file, bool condone_absent) {
  config_file = expand_path(config_file);
  if (g_backends.empty())
    global_backend_add(molpro::backend(molpro::backend::default_name));
  if (!fs::exists(config_file)) {
    if (condone_absent)
      return;
    else
      throw std::runtime_error("Backend config file " + config_file + " not found");
  }
  pugi::xml_document doc;
  doc.load_file(config_file.c_str());
//  xmlpp::DomParser dp(config_file);
//  auto doc = dp.get_document();
//  auto backends = doc->get_root_node()->find("/backends/backend");
  auto backends = doc.select_nodes("/backends/backend");
  for (const auto& be : backends) {
    auto name = getattribute(be, "name");
    global_backend_add(backend(name));
    std::string val;
    if ((val = getattribute(be, "template")) != "") {
      g_backends[name].host = g_backends[val].host;
      g_backends[name].cache = g_backends[val].cache;
      g_backends[name].run_command = g_backends[val].run_command;
      g_backends[name].run_jobnumber = g_backends[val].run_jobnumber;
      g_backends[name].status_command = g_backends[val].status_command;
      g_backends[name].status_running = g_backends[val].status_running;
      g_backends[name].status_waiting = g_backends[val].status_waiting;
      g_backends[name].kill_command = g_backends[val].kill_command;
    }
    if ((val = getattribute(be, "host")) != "") g_backends[name].host = val;
    if ((val = getattribute(be, "cache")) != "") g_backends[name].cache = val;
    if ((val = getattribute(be, "run_command")) != "") g_backends[name].run_command = val;
    if ((val = getattribute(be, "run_jobnumber")) != "") g_backends[name].run_jobnumber = val;
    if ((val = getattribute(be, "status_command")) != "") g_backends[name].status_command = val;
    if ((val = getattribute(be, "status_running")) != "") g_backends[name].status_running = val;
    if ((val = getattribute(be, "status_waiting")) != "") g_backends[name].status_waiting = val;
    if ((val = getattribute(be, "kill_command")) != "") g_backends[name].kill_command = val;
  }
//  for (const auto& be: g_backends)
//    std::cout << be.second.str() << std::endl;
}

std::string molpro::backend::str() const {
  std::stringstream ss;
  ss << "molpro backend " << name <<": "
     << " host=\"" << host << "\""
     << " cache=\"" << cache << "\""
     << " run_command=\"" << run_command << "\""
     << " status_command=\"" << status_command << "\""
     << " kill_command=\"" << kill_command << "\""
     ;
  return ss.str();
}

