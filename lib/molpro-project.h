#ifndef MOLPRO_PROJECT_MOLPRO_PROJECT_H
#define MOLPRO_PROJECT_MOLPRO_PROJECT_H
#ifdef __cplusplus
#include <vector>
#include <string>
#include <vector>
#include <memory>

namespace molpro {
class backend;
class pugi_xml_document;
backend& global_backend(std::string name);
static const std::string defaultSuffix = "molpro";
static const std::vector<std::string> default_reserved_files{"Info.plist", "molpro.rc", "%.inp", "%.pqb", "%.xyz"};
static const std::string recentProjectsFile = "~/.molpro/projects";
static constexpr int recentMax = 128;
enum status : int {
  unknown = 0,
  running = 1,
  waiting = 2,
  completed = 3,
};
class project {
  std::string
      m_filename; ///< the name of the file bundle, expressed as an absolute pathname for the directory holding the bundle
  std::vector<std::reference_wrapper<const backend> > m_backend;
//  int m_jobnumber;
  std::vector<std::string> m_reserved_files; ///< Files which should never be copied back from backend
  bool m_erase_on_destroy;
  std::unique_ptr<pugi_xml_document> m_properties;
 public:
  static const std::string s_propertyFile;
  /*!
   * @brief Construct, or attach to, a Molpro project bundle
   * @param filename The file name of the bundle. If it does not have suffix .molpro, it will be forced to do so.
   * @param source If not null, copy the bundle source. This is only for new projects, and an exception is thrown if *this has been attached to an existing project bundle.
   * @param erase_on_destroy If true, and the project was created new, then the destructor will destroy the disk copy of the project.
   * @param construct if false, do not actually build the project on disk. Can be used to generate the filename of the project.
   */
  explicit project(const std::string& filename,
                   const project* source = nullptr,
                   bool erase_on_destroy = false,
                   bool construct = true);
  project(project&& source) = default;
  ~project();
  /*!
   * @brief Copy the project to another location
   * @param destination_filename
   * @param keep_hash whether to clone the project_hash, or allow a fresh one to be generated
   * @return true if the copy was successful
   */
  bool copy(const std::string& destination_filename, bool keep_hash = false);
  /*!
   * @brief Move the project to another location
   * @param destination_filename
   * @return true if the move was successful
   */
  bool move(const std::string& destination_filename);
  /*!
   * @brief Destroy the project
  */
  void erase();
  static void erase(const std::string& filename) {
//    std::cerr << "molpro::project::erase "<<filename<<std::endl;
    project x(filename, nullptr, true, false);
  }
  /*!
   * @brief Import one or more files into the project. In the case of a .xml output file, if the corresponding
   * input file does not exist, it will be generated.
   * @param file
   * @param overwrite Whether to overwrite an existing file.
   */
  bool Import(std::string file, bool overwrite=false);
  bool Import(const std::vector<std::string> files, bool overwrite=false) {
    bool result;
    for (const auto& file : files) result &= Import(file, overwrite);
    return result;
  }
  /*!
   * @brief Export one or more files from the project.
   * @param file The relative or absolute path name of the destination; the base name will be used to locate the file in the project.
   * @param overwrite Whether to overwrite an existing file.
   */
  bool Export(std::string file, bool overwrite=false);
  bool Export(const std::vector<std::string> &files, bool overwrite=false) {
    bool result;
    for (const auto& file : files) result &= Export(file, overwrite);
    return result;
  }
  /*!
   * @brief Synchronize the project with a cached copy belonging to a backend. name.inp, name.xyz, Info.plist, and any files brought in with import(), will be pushed from the
   * master copy to the backend, and all other files will be pulled from the backend.
   * @param name
   * @param verbosity If >0, show underlying processing
   * @return
   */
  bool synchronize(std::string name, int verbosity = 0) { return synchronize(global_backend(name), verbosity); }
  bool synchronize(const backend& backend, int verbosity = 0);
  /*!
   * @brief Start a molpro job
   * @param name The name of the backend
   * @param options Any options to be passed to the command run on the backend
   * @param verbosity If >0, show underlying processing
   * @return
   */
  bool run(std::string name, std::vector<std::string> options = std::vector<std::string>{}, int verbosity = 0) {
    return run(global_backend(name), options, verbosity);
  }
  bool run(const backend& backend, std::vector<std::string> options = std::vector<std::string>{}, int verbosity = 0);
  /*!
   * @brief Obtain the status of the job started by run()
   * @param verbosity
   * - 0 print nothing
   * - 1 show result from underlying status commands
   * @return
   * - 0 not found
   * - 1 running
   * - 2 queued
   */
  molpro::status status(int verbosity = 0);
  /*!
   * @brief Kill the job started by run()
   * @return
   */
  void kill();
  /*!
   * @brief Remove potentially unwanted files from the project
   * @param oldOutput Whether to remove old output files
   * @param output Whether to remove all output files
   * @param unused Whether to remove unused files
   */
  void clean(bool oldOutput=true, bool output=false, bool unused=false);
  /*!
   * @brief Set a variable
   * @param property
   * @param value
   */
  void property_set(const std::string& property, const std::string& value);
  /*!
   * @brief Get the value of a variable
   * @param property
   * @return The value, or "" if key does not exist
   */
  std::string property_get(const std::string& property) const;
  /*!
   * @brief Remove a variable
   * @param property
   */
  void property_delete(const std::string& property);
  /*!
   * @brief Set the pointer for property_next() to the beginning of the list of variables.
   */
  void property_rewind();
  /*!
   * @brief Get the sequentially next variable.
   * @return
   */
  std::string property_next();
  /*!
   * @brief Get the file name of the bundle, or a primary file of particular type, or a general file in the bundle
   * @param suffix If present without \c name, look for a primary file with that type. If absent, the file name of the bundle is instead selected
   * @param name If present,  look for a file of this name, appended with .\c suffix if that is non-blank
   * @return the fully-qualified name of the file
   */
  std::string filename(const std::string& suffix = "", const std::string& name="") const;
  /*!
   * @brief
   * @return the base name of the project, ie its file name with directory and suffix stripped off
   */
  std::string name() const;
  /*!
   * @brief Look for a project by name in the user-global recent project list
   * @param filename
   * @return 0 if failure, otherwise the rank of the project (1 is newest)
   */
  static int recent_find(const std::string& filename);
  /*!
   * @brief Look for a project by rank in the user-global recent project list
   * @param number the rank of the project (1 is newest)
   * @return the filename of the project, or "" if not found
   */
  static std::string recent(int number = 1);
 protected:
  static void recent_edit(const std::string& add, const std::string& remove = "");
  void save_property_file();
  std::string cache(const backend& backend) const;
  void force_file_names(const std::string& oldname);
  std::string propertyFile();
 public:
  /*
   */
  /*!
   * @brief Return a globally-unique hash to identify the project.
   * The hash is generated on first call to this function, and should be relied
   * on to never change, including after calling move(). copy() normally results
   * in a new hash string, but this can be overridden if an exact clone is really wanted.
   * @return hash
   */
  size_t project_hash();
  /*!
   * @brief  Construct a hash that is unique to the contents of the input file and anything it references.
   * @return hash
   */
  size_t input_hash();

};

/*!
 * @brief Edit a file path name
 * - expand environment variables
 * - expand ~ as the user home directory
 * - map unix-specific environment variables HOME, TMPDIR to native counterparts
 * - force all path separators (/,\) to native form
 * - resolve a relative input path to its fully-resolved form using the current working directory
 * - if no trailing suffix, add a default
 * @param path
 * @param default_suffix If given and not empty, if path does not contain a file suffix give it one.
 * @return the expanded and sanitised path
 */
std::string expand_path(const std::string& path, const std::string& default_suffix = "");
}

extern "C" {
#else // __cplusplus
#endif // __cplusplus
int molpro_project_open(const char* project);
void molpro_project_close(const char* project);
int molpro_project_copy(const char* project, const char* destination_filename, int keep_hash);
int molpro_project_move(const char* project, const char* destination_filename);
void molpro_project_erase(const char* project);
void molpro_project_property_erase(const char* project);
int molpro_project_import(const char* project, const char* file);
int molpro_project_export(const char* project, const char* file);
int molpro_project_synchronize(const char* project, const char* backend, int verbosity);
int molpro_project_run(const char* project, const char* backend, const char* options, int verbosity);
int molpro_project_status(const char* project, int verbosity);
void molpro_project_kill(const char* project);
void molpro_project_property_set(const char* project, const char* key, const char* value);
char* molpro_project_property_get(const char* project, const char* key);
void molpro_project_property_delete(const char* project, const char* key);
void molpro_project_property_rewind(const char* project);
char* molpro_project_property_next(const char* project);
char* molpro_project_filename(const char* project);
char* molpro_project_name(const char* project);
size_t molpro_project_project_hash(const char* project);
size_t molpro_project_input_hash(const char* project);
int molpro_project_recent_find(const char* filename);
char* molpro_project_recent(int number);
char** molpro_global_backends();
char* molpro_expand_path(const char* path, const char* default_suffix);

#ifdef __cplusplus
}
#endif // __cplusplus

#endif //MOLPRO_PROJECT_MOLPRO_PROJECT_H
