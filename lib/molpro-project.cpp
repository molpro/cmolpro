#include "molpro-project.h"
#include <string>
#include <iostream>
#include <boost/algorithm/string.hpp>
#include <map>
#include <array>
#include <regex>
#include "molpro-backend.h"
#include <boost/process/search_path.hpp>
#include <boost/process/child.hpp>
#include <boost/process/spawn.hpp>
#include <boost/process/args.hpp>
#include <boost/process/io.hpp>
#include <boost/interprocess/sync/file_lock.hpp>
#include <boost/filesystem.hpp>
#include <pugixml.hpp>
#include <unistd.h>
#include <ctype.h>
#include <functional>
#if defined(__linux__) || defined(__APPLE__)
#include <sys/types.h>
#include <sys/wait.h>
#endif

namespace bp = boost::process;
namespace fs = boost::filesystem;

struct molpro::pugi_xml_document : public pugi::xml_document {};

const std::string molpro::project::s_propertyFile = "Info.plist";

inline fs::path executable(fs::path command) {
  if (command.is_absolute())
    return command;
  else
    return bp::search_path(command);
}

bool copyDir(
    fs::path const& source,
    fs::path const& destination,
    bool delete_source = false
) {
  // Check whether the function call is valid
  if (!fs::exists(source) || !fs::is_directory(source))
    throw std::runtime_error("Source directory " + source.string() + " does not exist or is not a directory.");
  if (fs::exists(destination))
    throw std::runtime_error("Destination directory " + destination.string() + " already exists.");
  // Create the destination directory
  if (!fs::create_directory(destination))
    throw std::runtime_error("Unable to create destination directory " + destination.string());
  // Iterate through the source directory
  for (
      fs::directory_iterator file(source);
      file != fs::directory_iterator(); ++file
      ) {
    fs::path current(file->path());
    if (fs::is_directory(current)) {
      // Found directory: Recursion
      if (
          !copyDir(
              current,
              destination / current.filename(),
              delete_source
          )
          ) {
        return false;
      }
    } else {
      // Found file: Copy
      fs::copy_file(
          current,
          destination / current.filename()
      );
    }
  }
  return true;
}

namespace molpro {

project::project(const std::string& filename, const project* source, bool erase_on_destroy, bool construct) :
    m_filename(expand_path(filename, "molpro")),
    m_reserved_files(default_reserved_files),
    m_erase_on_destroy(erase_on_destroy),
    m_properties(std::make_unique<pugi_xml_document>()) {
  if (!fs::exists(m_filename))
    fs::create_directories(m_filename);
  else if (construct)
    m_erase_on_destroy = false;
//  std::cout << fs::system_complete(m_filename) << std::endl;
  if (!fs::exists(m_filename))
    throw std::runtime_error("project does not exist and could not be created: " + m_filename);
  if (!fs::is_directory(m_filename))
    throw std::runtime_error("project should be a directory: " + m_filename);

//  std::cerr << "constructor m_filename="<<m_filename<<" destroy="<<erase_on_destroy<<m_erase_on_destroy<<" construct="<<construct<<std::endl;
  if (!construct) return;
  if (!fs::exists(propertyFile())) {
    save_property_file();
    property_set("backend_inactive", "1");
    property_set("backend_inactive_synced", "0");
  }
  if (!m_properties->load_file(propertyFile().c_str()))
    throw std::runtime_error("error in loading " + propertyFile());

  auto nimport = property_get("IMPORTED").empty() ? 0 : std::stoi(property_get("IMPORTED"));
//    std::cerr << "nimport "<<nimport<<std::endl;
  for (int i = 0; i < nimport; i++) {
//      std::cerr << "key "<<std::string{"IMPORT"}+std::to_string(i)<<", value "<<property_get(std::string{"IMPORT"}+std::to_string(i))<<std::endl;
    m_reserved_files.push_back(property_get(std::string{"IMPORT"} + std::to_string(i)));
  }
  recent_edit(m_filename);

}

project::~project() { if (m_erase_on_destroy) erase(); }

bool project::Import(std::string file, bool overwrite) {
  auto to = fs::path{m_filename};
  if (fs::path{file}.extension() == "inp")
    to /= fs::path{name()} / "inp";
  else if (fs::path{file}.extension() == "xml"
      and !fs::exists(fs::path{m_filename} / fs::path{name()}.replace_extension("inp"))) {
    to /= fs::path{file}.filename();
    //TODO: implement generation of .inp from .xml
  } else
    to /= fs::path{file}.filename();
  boost::system::error_code ec;
//  std::cerr << "Import copies from "<<file<<" to "<<to<<std::endl;
  if (overwrite and exists(to))
    remove(to);
  fs::copy_file(file, to, ec);
  m_reserved_files.push_back(to.string());
  auto nimport = property_get("IMPORTED").empty() ? 0 : std::stoi(property_get("IMPORTED"));
  std::string key = "IMPORT" + std::to_string(nimport);
  property_set(key, to.filename().string());
  property_set("IMPORTED", std::to_string(nimport + 1));
  if (ec) throw std::runtime_error(ec.message());
  return true;
}

bool project::Export(std::string file, bool overwrite) {
  if (!property_get("backend").empty())synchronize(molpro::global_backend(property_get("backend")), 0);
  auto from = fs::path{m_filename};
  from /= fs::path{file}.filename();
  boost::system::error_code ec;
  if (overwrite and exists(fs::path{file}))
    remove(fs::path{file});
  fs::copy_file(from, file, ec);
  if (ec) throw std::runtime_error(ec.message());
  return true;
}

std::string project::cache(const backend& backend) const {
  return backend.cache + "/" + m_filename; // TODO: use boost::filesystem to avoid '/' on windows
}

bool project::synchronize(const backend& backend, int verbosity) {
  if (verbosity > 1) std::cerr << "synchronize with " << backend.name << " (" << backend.host << ")" << std::endl;
  if (backend.host == "localhost") return true;
  if (verbosity > 1)
  std::cerr << "synchronize backend_inactive="<<property_get("backend_inactive") << " backend_inactive_synced="<<property_get("backend_inactive_synced")<<std::endl;
  if (property_get("backend_inactive_synced") == "1") return true;
  //TODO: implement more robust error checking
  fs::path current_path_save;
  try {
    current_path_save = fs::current_path();
  }
  catch (...) {
    current_path_save = "";
  }
  fs::current_path(m_filename);
  system(("ssh " + backend.host + " mkdir -p " + cache(backend)).c_str());
  // absolutely send reserved files
  std::string rfs;
  for (const auto& rf : m_reserved_files) {
//    std::cerr << "reserved file pattern " << rf << std::endl;
    auto f = regex_replace(rf, std::regex(R"--(%)--"), name());
//    std::cerr << "reserved file resolved " << f << std::endl;
    if (fs::exists(f))
      rfs += " " + f;
  }
  if (!rfs.empty())
    system(("rsync -L -a " + (verbosity > 0 ? std::string{"-v "} : std::string{""}) + rfs + " " + backend.host + ":"
        + cache(backend)).c_str());
  // send any files that do not exist on the backend yet
  system(("rsync -L --ignore-existing -a " + (verbosity > 0 ? std::string{"-v "} : std::string{""}) + ". "
      + backend.host
      + ":"
      + cache(backend)).c_str());
  // fetch all newer files from backend
  system(("rsync -a " + (verbosity > 0 ? std::string{"-v "} : std::string{""}) + backend.host + ":" + cache(backend)
      + "/ .").c_str());
  if (current_path_save != "")
    fs::current_path(current_path_save);
  status(0); // to get backend_inactive
//  std::cerr << "synchronize backend_inactive="<<property_get("backend_inactive")<<std::endl;
  property_set("backend_inactive_synced", property_get("backend_inactive"));
//  std::cerr << "synchronize backend_inactive_synced="<<property_get("backend_inactive_synced")<<std::endl;
  return true;
}

void project::force_file_names(const std::string& oldname) {

  fs::directory_iterator end_iter;
  for (fs::directory_iterator dir_itr(m_filename);
       dir_itr != end_iter;
       ++dir_itr) {
    auto path = dir_itr->path();
    try {
      if (path.stem() == oldname) {
        auto newpath = path.parent_path();
        newpath /= name();
        newpath.replace_extension(dir_itr->path().extension());
//        std::cerr << "rename(" << path.filename() << "," << newpath.filename() << ")" << std::endl;
        rename(path, newpath);

        if (newpath.extension() == ".inp") {
          std::ifstream in(newpath.string());
          std::string contents((std::istreambuf_iterator<char>(in)), std::istreambuf_iterator<char>());
          boost::replace_all(contents, oldname + ".xyz", name() + ".xyz");
          std::ofstream out(newpath.string());
          out << contents;
        }
      }
    }
    catch (const std::exception& ex) {
      throw std::runtime_error(dir_itr->path().leaf().native() + " " + ex.what());
    }
  }

  property_rewind();
  std::string key = property_next();
  for (; !key.empty(); key = property_next()) {
    auto value = property_get(key);
    boost::replace_first(value, oldname + ".", name() + ".");
    property_set(key, value);
  }
}

std::string project::propertyFile() { return (fs::path{m_filename} / fs::path{s_propertyFile}).string(); }

bool project::move(const std::string& destination_filename) {
  auto stat = status(-1);
  if (stat == running or stat == waiting) return false;
  auto dest = fs::absolute(expand_path(destination_filename)).replace_extension(".molpro");
//  std::cerr << "move to "<<dest<<std::endl;
  if (!property_get("backend").empty()) synchronize(molpro::global_backend(property_get("backend")), 0);
  auto namesave = name();
  auto filenamesave = m_filename;
  if (copyDir(fs::path(m_filename), dest, true)) {
    m_filename = dest.string();
    force_file_names(namesave);
    recent_edit(m_filename, filenamesave);
    return fs::remove_all(filenamesave) > 0;
  }
  return false;
}

bool project::copy(const std::string& destination_filename, bool keep_hash) {
  auto dest = fs::absolute(expand_path(destination_filename)).replace_extension(".molpro");
  if (!property_get("backend").empty()) synchronize(molpro::global_backend(property_get("backend")), 0);
  copyDir(fs::path(m_filename), dest, false);
  project dp(dest.string());
  dp.force_file_names(name());
  recent_edit(dp.m_filename);
  dp.property_delete("jobnumber");
  fs::remove_all(fs::path{dp.m_filename} / fs::path{dp.name() + ".d"});
  fs::remove_all(fs::path{dp.m_filename} / fs::path{dp.name() + ".out"});
  fs::remove_all(fs::path{dp.m_filename} / fs::path{dp.name() + ".xml"});
  for (int i = 0; i < 100; ++i) {
    fs::remove_all(fs::path{dp.m_filename} / fs::path{dp.name() + ".out_" + std::to_string(i)});
    fs::remove_all(fs::path{dp.m_filename} / fs::path{dp.name() + ".xml_" + std::to_string(i)});
  }
  if (!keep_hash)
    dp.property_delete("project_hash");
  return true;
}

void project::erase() {
  if (fs::remove_all(m_filename)) {
    recent_edit("", m_filename);
    m_filename = "";
  }
}

static std::vector<std::string> splitString(std::string input, char c = ' ') {
  std::vector<std::string> result;
  const char* str0 = strdup(input.c_str());
  const char* str = str0;
  do {
    while (*str == c && *str) ++str;
    const char* begin = str;
    while (*str != c && *str) ++str;
    result.push_back(std::string(begin, str));
    if (result.back().empty()) result.pop_back();
  } while (0 != *str++);
  free((void*) str0);
  return result;
}

bool project::run(const backend& backend, std::vector<std::string> options, int verbosity) {
  if (status(verbosity) != unknown && status(0) != completed) return false;
  property_set("backend", backend.name);
  fs::path current_path_save;
  try {
    current_path_save = fs::current_path();
  }
  catch (...) {
    current_path_save = "";
  }
  fs::current_path(m_filename);
  std::string line;
  bp::child c;
  std::string optionstring;
  for (const auto& o : options) optionstring += o + " ";
  property_set("run_options", optionstring);
  if (verbosity > 0) optionstring += "-v ";
  if (backend.host == "localhost") {
    property_set("backend_inactive", "0");
    property_set("backend_inactive_synced", "0");
    if (verbosity > 0) std::cerr << "run local job, backend=" << backend.name << std::endl;
    if (verbosity > 1)
      std::cerr << executable(backend.run_command) << " " << optionstring << " " << fs::path(name() + ".inp")
                << std::endl;
    if (verbosity > 2)
      for (const auto& o : splitString(optionstring))
        std::cerr << "option " << o << std::endl;
    if (optionstring.empty())
      c = bp::child(executable(backend.run_command),
                    fs::path(name() + ".inp"));
    else
      c = bp::child(executable(backend.run_command),
                    bp::args(splitString(optionstring)),
                    fs::path(name() + ".inp"));
    auto result = c.running();
    c.detach();
    property_set("jobnumber", std::to_string(c.id()));
    if (verbosity > 1) std::cerr << "jobnumber " << c.id() << ", running=" << c.running() << std::endl;
    fs::current_path(current_path_save);
    return result;
  } else { // remote host
    if (verbosity > 0) std::cerr << "run remote job on " << backend.name << std::endl;
    bp::ipstream c_err, c_out;
    synchronize(backend, verbosity);
    property_set("backend_inactive", "0");
    property_set("backend_inactive_synced", "0");
    if (verbosity > 3) std::cerr << "cache(backend) " << cache(backend) << std::endl;
    auto jobstring =
        "cd " + cache(backend) + "; nohup " + backend.run_command + " " + optionstring
            + name()
            + ".inp& echo $! ";
    if (verbosity > 3) std::cerr << "jobstring " << jobstring << std::endl;
    c = bp::child(bp::search_path("ssh"), backend.host, jobstring, bp::std_err > c_err, bp::std_out > c_out);
    std::string sstdout, sstderr;
    if (verbosity > 2)
      std::cerr << "examine job submission output against regex: " << backend.run_jobnumber << std::endl;
    while (std::getline(c_out, line)) {
      sstdout += line + "\n";
      std::smatch match;
      if (verbosity > 1)
        std::cerr << line << std::endl;
      if (std::regex_search(line, match, std::regex{backend.run_jobnumber})) {
        if (verbosity > 2)
          std::cerr << "... a match was found: " << match[1] << std::endl;
        if (verbosity > 1) status(verbosity - 2);
        property_set("jobnumber", match[1]);
        fs::current_path(current_path_save);
        return true;
      }
    }
    while (std::getline(c_err, line))
      sstderr += line + "\n";
    std::cerr << "Remote job number not captured for backend \"" + backend.name + "\":\n" << sstdout << "\n" << sstderr
              << std::endl;
  }
  if (current_path_save != "")
    fs::current_path(current_path_save);
  return false;
}

void project::clean(bool oldOutput, bool output, bool unused) {
  if (oldOutput or output) {
    fs::remove_all(fs::path{filename()} / fs::path{name() + ".d"});
  }
  if (output) {
    fs::remove(fs::path{filename()} / fs::path{name() + ".out"});
    fs::remove(fs::path{filename()} / fs::path{name() + ".xml"});
  }
  if (unused)
    throw std::runtime_error("molpro::project::clean for unused files is not yet implemented");
}

void project::kill() {
  if (property_get("backend").empty()) return;
  auto be = global_backend(property_get("backend"));
  auto pid = property_get("jobnumber");
  if (pid.empty()) return;
  if (be.host == "localhost") {
    auto spacepos = be.kill_command.find_first_of(" ");
    if (spacepos != std::string::npos)
      bp::spawn(executable(be.kill_command.substr(0, spacepos)),
                be.kill_command.substr(spacepos + 1, std::string::npos), pid);
    else
      bp::spawn(executable(be.kill_command), pid);
  } else {
//    std::cerr << "remote kill "<<be.host<<":"<<be.kill_command<<":"<<pid<<std::endl;
    bp::spawn(bp::search_path("ssh"), be.host, be.kill_command, pid);
  }
}

status project::status(int verbosity) {
  if (property_get("backend").empty()) return unknown;
  auto be = global_backend(property_get("backend"));
  auto pid = property_get("jobnumber");
  if (pid.empty()) return unknown;
  property_set("backend_inactive", "0");
  if (be.host == "localhost") {
//    std::cerr << "status of local job " << pid << std::endl;
    auto spacepos = be.status_command.find_first_of(" ");
    bp::child c;
    bp::ipstream is;
    std::string line;
    auto cmd = splitString(be.status_command + " " + pid);
    c = bp::child(
        executable(cmd[0]), bp::args(std::vector<std::string>{cmd.begin() + 1, cmd.end()}),
        bp::std_out > is);
    while (std::getline(is, line)) {
      while (isspace(line.back())) line.pop_back();
//      std::cout << "line: @"<<line<<"@"<<std::endl;
      if ((" " + line).find(" " + pid + " ") != std::string::npos) {
        if (line.back() == 'Z') { // zombie process
#if defined(__linux__) || defined(__APPLE__)
          waitpid(std::stoi(pid), NULL, WNOHANG);
#endif
	  return completed;
        }
        return running;
      }
    }
  } else {
//    std::cerr << "remote status "<<be.host<<":"<<be.status_command<<":"<<pid<<std::endl;
    bp::ipstream pstderr, pstdout;
    std::string line;
    bp::spawn(bp::search_path("ssh"), be.host, be.status_command, pid, bp::std_out > pstdout, bp::std_err > pstderr);
    while (std::getline(pstdout, line)) {
      if (verbosity > 0) std::cout << line << std::endl;
      if ((" " + line).find(" " + pid + " ") != std::string::npos) {
        std::smatch match;
        if (verbosity > 2) std::cerr << "line" << line << std::endl;
        if (verbosity > 2) std::cerr << "status_running " << be.status_running << std::endl;
        if (verbosity > 2) std::cerr << "status_waiting " << be.status_waiting << std::endl;
        if (std::regex_search(line, match, std::regex{be.status_running})) return running;
        if (std::regex_search(line, match, std::regex{be.status_waiting})) return waiting;
      }
    }
  }
  auto result = property_get("jobnumber") == "" ? unknown : completed;
  synchronize(be);
  property_set("backend_inactive",
               (result != completed or
    fs::exists(fs::path{filename()} / fs::path{name() + ".xml"}) // there may be a race, where the job status is completed, but the output file is not yet there. This is an imperfect test for that .. just whether the .xml exists at all. TODO: improve test for complete output file
               )
      ? "1" : "0");
  return result;
}

static int property_sequence;
void project::property_rewind() {
  property_sequence = 0;
}

void project::property_delete(const std::string& property) {
//  std::cerr << "delete " << property << std::endl;
  if (!m_properties->child("plist")) m_properties->append_child("plist");
  if (!m_properties->child("plist").child("dict")) m_properties->child("plist").append_child("dict");
  auto dict = m_properties->child("plist").child("dict");
  std::string query{"//key[text()='" + property + "']"};
  auto nodes = dict.select_nodes(query.c_str());
  for (const auto& keynode : nodes) {
    auto valnode = keynode.node().select_node("following-sibling::string[1]");
    dict.remove_child(keynode.node());
    dict.remove_child(valnode.node());
  }
  save_property_file();
}

void project::property_set(const std::string& property, const std::string& value) {
//  std::cerr << "set " << property << ":" << value << std::endl;
  property_delete(property);
  {
    if (!m_properties->child("plist")) m_properties->append_child("plist");
    if (!m_properties->child("plist").child("dict")) m_properties->child("plist").append_child("dict");
    auto keynode = m_properties->child("plist").child("dict").append_child("key");
    keynode.text() = property.c_str();
    auto stringnode = m_properties->child("plist").child("dict").append_child("string");
    stringnode.text() = value.c_str();
    save_property_file();
  }
}

std::string project::property_get(const std::string& property) const {
//  std::cerr << "get " << property << std::endl;
  std::string query{"/plist/dict/key[text()='" + property + "']/following-sibling::string[1]"};
  return m_properties->select_node(query.c_str()).node().child_value();
}

std::string project::property_next() {
  property_sequence++;
  std::string query{"/plist/dict/key[position()='" + std::to_string(property_sequence) + "']"};
  return m_properties->select_node(query.c_str()).node().child_value();
}

void project::recent_edit(const std::string& add, const std::string& remove) {
  std::string rf{expand_path(recentProjectsFile)};
  bool changed = false;
  {
    if (!fs::exists(rf)) {
      fs::create_directories(fs::path(rf).parent_path());
      fs::ofstream junk(rf);
    }
    std::ifstream in(rf);
    std::ofstream out(rf + "-");
    size_t lines = 0;
    if (!add.empty()) {
      out << add << std::endl;
      changed = true;
      ++lines;
    }
    std::string line;
    while (in >> line && lines < recentMax) {
      if (line != remove && line != add) {
        ++lines;
        out << line << std::endl;
      } else
        changed = true;
    }
    changed = changed or lines >= recentMax;
  }
  if (changed)
    fs::rename(rf + "-", rf);
  else
    fs::remove(rf + "-");
}

std::string project::filename(const std::string& suffix, const std::string& name) const {
  if (suffix != "" and name == "")
    return m_filename + "/" + fs::path(m_filename).stem().string() + "." + suffix;
  else if (suffix != "" and name != "")
    return m_filename + "/" + name + "." + suffix;
  else if (name != "")
    return m_filename + "/" + name;
  else
    return m_filename;
}
std::string project::name() const { return fs::path(m_filename).stem().string(); }

int project::recent_find(const std::string& filename) {
  std::string rf{expand_path(recentProjectsFile)};
  std::ifstream in(rf);
  std::string line;
  for (int position = 1; in >> line; ++position)
    if (line == filename) return position;
  return 0;
}

std::string project::recent(int number) {
  std::string rf{expand_path(recentProjectsFile)};
  std::ifstream in(rf);
  std::string line;
  for (int position = 1; in >> line; ++position)
    if (position == number) return line;
  return "";

}

struct plist_writer : pugi::xml_writer {
  std::string file;
  virtual void write(const void* data, size_t size) {
    std::ofstream s(file);
    s << "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"
      << "<!DOCTYPE plist SYSTEM '\"-//Apple//DTD PLIST 1.0//EN\" \"http://www.apple.com/DTDs/PropertyList-1.0.dtd\"'>\n"
      << std::string{static_cast<const char*>(data), size};
  }
};

constexpr static bool use_writer = false;

void project::save_property_file() {
//  std::cout << "save_property_file" << std::endl;
//  system((std::string{"cat "} + propertyFile()).c_str());
  struct plist_writer writer;
  writer.file = propertyFile();
  try {
    boost::interprocess::file_lock fileLock{writer.file.c_str()};
    fileLock.lock();
    if (use_writer)
      m_properties->save(writer, "\t", pugi::format_no_declaration);
    else
      m_properties->save_file(propertyFile().c_str());
  }
  catch (boost::interprocess::interprocess_exception& x) {
    if (use_writer)
      m_properties->save(writer, "\t", pugi::format_no_declaration);
    else
      m_properties->save_file(propertyFile().c_str());
  }
//  system((std::string{"cat "} + propertyFile()).c_str());
//  std::cout << "end save_property_file" << std::endl;
}

inline std::string random_string(size_t length) {
  auto randchar = []() -> char {
    const char charset[] =
        "0123456789"
        "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
        "abcdefghijklmnopqrstuvwxyz";
    const size_t max_index = (sizeof(charset) - 1);
    return charset[rand() % max_index];
  };
  std::string str(length, 0);
  std::generate_n(str.begin(), length, randchar);
  return str;
}

size_t molpro::project::project_hash() {
  auto p = this->property_get("project_hash");
  size_t result;
  if (p.empty()) {
    result = std::hash<std::string>{}(random_string(32));
    this->property_set("project_hash", std::to_string(result));
  } else {
    std::istringstream iss(p);
    iss >> result;
  }
  return result;
}

size_t molpro::project::input_hash() {
//  std::cout << (filename("inp")) << std::endl;
  std::ifstream ss(filename("inp"));
  std::string line;
  std::string input;
  while (std::getline(ss, line)) {
//    std::cout << "line=" << line << std::endl;
    auto pos = line.find("geometry=");
    if (pos != std::string::npos && line[pos + 1] != '{') {
      auto fn = filename("", line.substr(pos + 9));
//      system((std::string{"ls -l "} + fn).c_str());
//      std::cout<<"parse geometry file "<<fn<<std::endl;
      std::ifstream s2(fn);
      input += std::string(std::istreambuf_iterator<char>(s2),
                           std::istreambuf_iterator<char>());
    } else
      input += line + "\n";
    // TODO add contents of other referenced files too
  }
//  std::cout << "input_hash: @" << input << "@" << std::endl;
//  std::cout << std::hash<std::string>{}(input) << std::endl;
  return std::hash<std::string>{}(input);
//  return std::hash<std::string>{}(std::string(std::istreambuf_iterator<char>(ss),std::istreambuf_iterator<char>())); // TODO add contents of referenced files too
}

inline std::string environment(std::string key) {
  char* s = std::getenv(key.c_str());
  if (s == nullptr) {
    if (key == "TMPDIR")
      return "/tmp";
    throw std::runtime_error("Unknown environment variable " + key);
  }
  return s;
}

std::string expand_path(const std::string& path, const std::string& default_suffix) {
  auto text = path;
  // specials
#ifdef _WIN32
  text = std::regex_replace(text,std::regex{R"--(\~)--"},environment("USERPROFILE"));
  text = std::regex_replace(text,std::regex{R"--(\$\{HOME\})--"},environment("USERPROFILE"));
  text = std::regex_replace(text,std::regex{R"--(\$HOME/)--"},environment("USERPROFILE")+"/");
  text = std::regex_replace(text,std::regex{R"--(\$\{TMPDIR\})--"},environment("TEMP"));
  text = std::regex_replace(text,std::regex{R"--(\$TMPDIR/)--"},environment("TEMP")+"/");
#else
  text = std::regex_replace(text, std::regex{R"--(\~)--"}, environment("HOME"));
#endif
  // expand environment variables
  std::smatch match;
  while (std::regex_search(text, match, std::regex{R"--(\$([^{/]+)/)--"}))
    text.replace((match[0].first - text.cbegin()), (match[0].second - text.cbegin()), environment(match[1]) + "/");
  while (std::regex_search(text, match, std::regex{R"--(\$\{([^}]+)\})--"}))
    text.replace((match[0].first - text.cbegin()), (match[0].second - text.cbegin()), environment(match[1]));
  // replace separators by native form
  text = std::regex_replace(text, std::regex{R"--([/\\])--"}, std::string{fs::path::preferred_separator});
  // resolve relative path
  if (text[0] != fs::path::preferred_separator and text[1] != ':') {
    text = (fs::current_path() / text).string();
  }
  // remove any trailing slash
  if (text.back() == fs::path::preferred_separator) text.pop_back();
  // add suffix
  if (fs::path{text}.extension().empty() and !default_suffix.empty()) text += "." + default_suffix;
  return text;
}

static std::map<std::string, molpro::project> projects;

static void error(std::exception& e) {
  std::cerr << "Exception: " << e.what() << std::endl;
}
extern "C" {

int molpro_project_open(const char* project) {
  try {
    if (projects.count(project) > 0)
      throw std::runtime_error(std::string{"Attempt to open already-registered molpro_project "} + project);
    projects.emplace(std::make_pair(std::string{project}, molpro::project(project)));
    return 1;
  }
  catch (std::exception& e) { error(e); }
  catch (...) { }
  return 0;
}
void molpro_project_close(const char* project) {
  try {
    if (projects.count(project) > 0) projects.erase(project);
  }
  catch (std::exception& e) { error(e); }
  catch (...) { }
}
int molpro_project_copy(const char* project, const char* destination_filename, int keep_hash) {
  try {
    if (projects.count(project) == 0) molpro_project_open(project);
    return (projects.at(project).copy(destination_filename, keep_hash != 0) ? 1 : 0);
  }
  catch (std::exception& e) { error(e); }
  catch (...) { }
  return 0;
}
int molpro_project_move(const char* project, const char* destination_filename) {
  try {
    if (projects.count(project) == 0) molpro_project_open(project);
    auto success = projects.at(project).move(destination_filename);
    molpro_project_close(project);
    return (success ? 1 : 0);
  }
  catch (std::exception& e) { error(e); }
  catch (...) { }
  return 0;
}
void molpro_project_erase(const char* project) {
  try {
    if (projects.count(project) == 0) molpro_project_open(project);
    projects.at(project).erase();
    molpro_project_close(project);
  }
  catch (std::exception& e) { error(e); }
  catch (...) { }
}
int molpro_project_import(const char* project, const char* file) {
  try {
    if (projects.count(project) == 0) molpro_project_open(project);
    return (projects.at(project).Import(file) ? 1 : 0);
  }
  catch (std::exception& e) { error(e); }
  catch (...) { }
  return 0;
}
int molpro_project_export(const char* project, const char* file) {
  try {
    if (projects.count(project) == 0) molpro_project_open(project);
    return (projects.at(project).Export(file) ? 1 : 0);
  }
  catch (std::exception& e) { error(e); }
  catch (...) { }
  return 0;
}
int molpro_project_synchronize(const char* project, const char* backend, int verbosity) {
  try {
    if (projects.count(project) == 0) molpro_project_open(project);
    return (projects.at(project).synchronize(backend, verbosity) ? 1 : 0);
  }
  catch (std::exception& e) { error(e); }
  catch (...) { }
  return 0;
}
int molpro_project_run(const char* project,
                       const char* backend,
                       const char* options,
                       int verbosity) {
  try {
    if (projects.count(project) == 0) molpro_project_open(project);
    return (projects.at(project).run(backend,
                                     std::vector<std::string>(1, options),
                                     verbosity) ? 1 : 0);
  }
  catch (std::exception& e) { error(e); }
  catch (...) { }
  return false;
}
int molpro_project_status(const char* project, int verbosity) {
  try {
    if (projects.count(project) == 0) molpro_project_open(project);
    return static_cast<int>(projects.at(project).status(verbosity));
  }
  catch (std::exception& e) { error(e); }
  catch (...) { }
  return 0;
}
void molpro_project_kill(const char* project) {
  try {
    if (projects.count(project) == 0) molpro_project_open(project);
    projects.at(project).kill();
  }
  catch (std::exception& e) { error(e); }
  catch (...) { }
}
void molpro_project_property_set(const char* project, const char* key, const char* value) {
  try {
    if (projects.count(project) == 0) molpro_project_open(project);
    projects.at(project).property_set(std::string{key}, std::string{value});
  }
  catch (std::exception& e) { error(e); }
  catch (...) { }
}
char* molpro_project_property_get(const char* project,
                                  const char* key) {
  try {
    if (projects.count(project) == 0) molpro_project_open(project);
    return strdup(projects.at(project).property_get(std::string{key}).c_str());
  }
  catch (std::exception& e) { error(e); }
  catch (...) { }
  return NULL;
}
void molpro_project_property_delete(const char* project, const char* key) {
  try {
    if (projects.count(project) == 0) molpro_project_open(project);
    projects.at(project).property_delete(std::string{key});
  }
  catch (std::exception& e) { error(e); }
  catch (...) { }
}
void molpro_project_property_rewind(const char* project) {
  try {
    if (projects.count(project) == 0) molpro_project_open(project);
    projects.at(project).property_rewind();
  }
  catch (std::exception& e) { error(e); }
  catch (...) { }
}
char* molpro_project_property_next(const char* project) {
  try {
    if (projects.count(project) == 0) molpro_project_open(project);
    return strdup(projects.at(project).property_next().c_str());
  }
  catch (std::exception& e) { error(e); }
  catch (...) { }
  return NULL;
}
char* molpro_project_filename(const char* project) {
  try {
    if (projects.count(project) == 0) molpro_project_open(project);
    return strdup(projects.at(project).filename().c_str());
  }
  catch (std::exception& e) { error(e); }
  catch (...) { }
  return NULL;
}
char* molpro_project_name(const char* project) {
  try {
    if (projects.count(project) == 0) molpro_project_open(project);
    return strdup(projects.at(project).name().c_str());
  }
  catch (std::exception& e) { error(e); }
  catch (...) { }
  return NULL;
}
size_t molpro_project_project_hash(const char* project) {
  try {
    if (projects.count(project) == 0) molpro_project_open(project);
    return projects.at(project).project_hash();
  }
  catch (std::exception& e) { error(e); }
  catch (...) { }
  return 0;
}
size_t molpro_project_input_hash(const char* project) {
  try {
    if (projects.count(project) == 0) molpro_project_open(project);
    return projects.at(project).input_hash();
  }
  catch (std::exception& e) { error(e); }
  catch (...) { }
  return 0;
}
int molpro_project_recent_find(const char* filename) {
  try {
    return molpro::project::recent_find(std::string(filename));
  }
  catch (std::exception& e) { error(e); }
  catch (...) { }
  return 0;
}
char* molpro_project_recent(int number) { return strdup(molpro::project::recent(number).c_str()); }
char** molpro_global_backends() {
  try {
    const auto results = molpro::global_backends();
    char** result = (char**) malloc((results.size() + 1) * sizeof(char*));
    size_t i = 0;
    for (const auto& backend : results) {
      result[i++] = strdup(backend.c_str());
    }
    result[i] = NULL;
    return result;
  }
  catch (std::exception& e) { error(e); }
  catch (...) { }
  return NULL;
}
char* molpro_expand_path(const char* path, const char* default_suffix) {
  try {
    return strdup(molpro::expand_path(std::string{path}, std::string{default_suffix}).c_str());
  }
  catch (std::exception& e) { error(e); }
  catch (...) { }
  return NULL;
}
}
}; // namespace molpro

