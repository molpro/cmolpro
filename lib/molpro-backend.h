#ifndef MOLPRO_PROJECT_MOLPRO_BACKEND_H
#define MOLPRO_PROJECT_MOLPRO_BACKEND_H

#include "molpro-project.h"
namespace molpro {

class backend {
 public:
  std::string name;
  std::string host;
  std::string cache;
  std::string run_command;
  std::string run_jobnumber;
  std::string status_command;
  std::string status_waiting;
  std::string status_running;
  std::string kill_command;
  static std::string default_name;
  backend(std::string name = default_name,
          std::string host = "localhost",
          std::string cache = "${PWD}",
          std::string run_command = "molpro",
          std::string run_jobnumber = "([0-9]+)",
          std::string status_command = "/bin/ps -o pid,state -p",
          std::string status_running = "^S$",
          std::string status_waiting = "^[^SZ]$",
          std::string kill_command = "pkill -P")
      : name(std::move(name)),
        host(std::move(host)),
        cache(std::move(cache)),
        run_command(std::move(run_command)),
        run_jobnumber(std::move(run_jobnumber)),
        status_command(std::move(status_command)),
        status_running(std::move(status_running)),
        status_waiting(std::move(status_waiting)),
        kill_command(std::move(kill_command)) {
  }
  std::string str() const;
};
/*!
 * @brief Get a backend defined in the global table of registered backends
 * @param name
 * @return
 */
backend& global_backend(std::string name = backend::default_name);
void global_backend_add(backend entry = backend{backend::default_name});
void global_backend_add(std::string config_file, bool condone_absent=false);
std::vector<std::string> global_backends();
}

#endif //MOLPRO_PROJECT_MOLPRO_BACKEND_H
