#include <iostream>
#include "molpro-project.h"
#include "molpro-backend.h"
#include <tclap/CmdLine.h>
#include <vector>
#include <map>
#include <string>
#include <cstdlib>
#include <boost/filesystem.hpp>

namespace fs = boost::filesystem;

int main(int argc, char* argv[]) {
  try {

    TCLAP::CmdLine cmd("Molpro project utility\nThis software is based on pugixml library (http://pugixml.org). pugixml is Copyright (C) 2006-2018 Arseny Kapoulkine.", ' ', MOLPRO_PROJECT_VERSION, true);

    std::vector<std::string> allowedCommands;
    std::string description{"specifies the action to be taken, and should be one of"};
    allowedCommands.push_back("import");
    description +=
        "\nimport: Following arguments give one or more files which should be copied into the project bundle.";
    allowedCommands.push_back("export");
    description +=
        "\nexport: Following arguments give one or more files which should be copied out of the project bundle. Each file can be prefixed with an absolute or relative directory name, default '.', which specifies where the file will be copied to";
    allowedCommands.push_back("new");
    description += "\nnew: Make a completely new project bundle; the following argument gives the destination";
    allowedCommands.push_back("copy");
    description += "\ncopy: Make a copy of the project bundle; the following argument gives the destination";
    allowedCommands.push_back("move");
    description += "\nmove:  Move the project bundle; the following argument gives the destination";
    allowedCommands.push_back("edit");
    description += "\nedit: Edit the Molpro input file using ${VISUAL} (default ${EDITOR}, default vi)";
    allowedCommands.push_back("browse");
    description += "\nbrowse: Browse the Molpro output file using ${PAGER} (default less)";
    allowedCommands.push_back("clean");
    description += "\nclean: Remove obsolete files";
    allowedCommands.push_back("run");
    description += "\nrun: Launch a job. Following arguments can specify any options to be given to the command run on the backend.";
    allowedCommands.push_back("status");
    description += "\nstatus: Report the status of the job launched by run";
    allowedCommands.push_back("property");
    description += "\nproperty: Report the value of a property stored in the project registry";
    allowedCommands.push_back("kill");
    description += "\nkill: Kill the job launched by run";
    allowedCommands.push_back("erase");
    description += "\nerase: Erase the project";
    allowedCommands.push_back("sync");
    description += "\nsync: Synchronize the project with the backend";
    TCLAP::ValuesConstraint<std::string>
        allowedVals(allowedCommands);
    TCLAP::MultiSwitchArg verboseSwitch("v", "verbose", "show detail", cmd, 0);
    TCLAP::UnlabeledValueArg<std::string> commandArg("command",
                                                     description, true,
                                                     "The subcommand",
                                                     &allowedVals);
    cmd.add(commandArg);
    auto v = new TCLAP::IgnoreRestVisitor();
    TCLAP::SwitchArg allArg("a","all","Apply to all projects, and ignore the project argument (but which has to be given, for syntax reasons)",false,v);
    cmd.add(allArg);
    std::string backend_description
        {"Specify the backend where jobs will be run. This should be the name field of one of the entries configured in"};
    std::vector<std::string> allowedBackends = molpro::global_backends();
    TCLAP::ValuesConstraint<std::string>
        allowedBackendsC(allowedBackends);
    backend_description += " /usr/local/etc/molpro/backends.xml";
    backend_description += " ~/.molpro/backends.xml";
    backend_description += ", or \"local\", meaning run on the local machine. Default: local";
    TCLAP::ValueArg<std::string>
        backendSwitch("b",
                   "backend",
                   backend_description,
                   false,
                   "local",
                   &allowedBackendsC,
                   cmd);
    TCLAP::SwitchArg forceArg("f","force","Allow operations that would result in overwriting an existing file",false,v);
    cmd.add(forceArg);
    TCLAP::UnlabeledValueArg<std::string>
        projectArg("project",
                   "Project file. It can be a relative or absolute path name, and its suffix, if absent, will be forced to be .molpro. If it does not exist, it will be created. If the -a flag is specified, the given project name is ignored, and instead the operation is carried out on all existing projects. Suffixes other than .molpro are treated as if they refer to the plain Molpro input file; in that case, a supporting project is constructed behind the scenes, in the directory ./.molpro-project, and a symbolic link to the corresponding output file is constructed.",
                   true,
                   "The project file",
                   "project-file",
                   cmd);

    TCLAP::UnlabeledMultiArg<std::string>
        extraArg("additional", "Additional subcommand-specific arguments", false, "additional arguments", cmd);

    cmd.parse( argc, argv );

    std::string project = projectArg.getValue();
    if (fs::path{project}.extension().empty())
      project += ".molpro";
    std::string command = commandArg.getValue();
    std::vector<std::string> extras = extraArg.getValue();
    if (verboseSwitch.getValue() > 0) {
      std::cout << "molpro-project " << command << " " << project;

      for (const auto& extra : extras)
        std::cout << " " << extra << std::endl;
      std::cout << std::endl;
    }
    if (extras.size() > 1 and (command != "import" and command != "export" and command != "run"))
      throw TCLAP::CmdLineParseException("Too many arguments on command line");
//    std::cerr << "project "<<project << "extension "<<fs::path{project}.extension()<<std::endl;
//    std::cerr << fs::is_regular_file(project) <<!fs::exists(project) << !fs::path{project}.extension().empty() << (fs::path{project}.extension() != ".molpro")<<std::endl;
    if (fs::is_regular_file(project)
        or (!fs::exists(project) and !fs::path{project}.extension().empty() and fs::path{project}.extension() != ".molpro")) { // assume that this is the input file
      auto given = fs::path{project};
      auto projectname = given.stem();
      fs::path projectlocation{"./.molpro-project"};
      fs::create_directories(projectlocation);
      project = (projectlocation / projectname).string() + ".molpro";
      molpro::project x(project); // to force its creation
      auto projectin = project / projectname.replace_extension(".inp");
//      std::cerr << "projectin="<<projectin<<fs::exists(projectin)<<std::endl;
      if (!fs::exists(projectin)){
//        std::cerr << "has root "<<given.has_root_directory()<<std::endl;
        auto givenr = !given.has_root_directory() ? fs::path{".."} / fs::path{".."} / given : given;
//        std::cerr << "given="<<given<<fs::exists(given)<<std::endl;
//        std::cerr << "givenr="<<givenr<<std::endl;
        if (!fs::exists(given)) {
          { auto o = std::ofstream(given.string()); }
          fs::create_symlink(givenr, projectin);
          fs::remove(given);
        } else
          fs::create_symlink(givenr, projectin);
      }
      auto out = given.parent_path() / projectname.replace_extension(".out");
      if (!fs::exists(out)) {
        fs::remove(out);
        auto projectout = project / projectname.replace_extension(".out");
        if (!fs::exists(projectout)) {
          { auto o = std::ofstream(projectout.string()); }
          fs::create_symlink(projectout, out);
          fs::remove(projectout);
        } else
          fs::create_symlink(projectout, out);
      }
    }
    if (fs::exists(fs::path{project})
        and !fs::is_regular_file(fs::path{project} / fs::path{molpro::project::s_propertyFile}))
      throw std::runtime_error("Existing project file is not a bundle");

    if (verboseSwitch.getValue()>1) {
      std::cout << "Defined backends: "<<std::endl;
      for (const auto& n : molpro::global_backends())
        std::cout << molpro::global_backend(n).str()<<std::endl;
    }
    std::map<molpro::status,std::string> status_message;
    status_message[molpro::status::unknown]="Not found";
    status_message[molpro::status::running]="Running";
    status_message[molpro::status::waiting]="Waiting";
    status_message[molpro::status::completed]="Completed";
    molpro::project proj(allArg.getValue() ? "$TMPDIR/.all" : project);
    if (verboseSwitch.getValue()>1)
      std::cout << "Project location: "<<proj.filename()<<std::endl;

    bool success = true;
    if (command == "import")
      success = proj.Import(extras,forceArg.getValue());
    else if (command == "export")
      success = proj.Export(extras,forceArg.getValue());
    else if (command == "new")
      ;
    else if (command == "copy")
      proj.copy(extras.front()); // TODO error trapping
    else if (command == "move")
      success = proj.move(extras.front());
    else if (command == "erase")
      proj.erase();
    else if (command == "property")
      for (const auto& key : extras)
        std::cout << "Property " << key << ": " << proj.property_get(key) << std::endl;
    else if (command == "status") {
      auto status = proj.status(verboseSwitch.getValue());
      std::cout << "Status: " << status_message[status];
      if (status != molpro::status::unknown && !proj.property_get("jobnumber").empty())
        std::cout << ", job number " << proj.property_get("jobnumber") << " on backend " << proj.property_get("backend");
      std::cout << std::endl;
    } else if (command == "kill")
      proj.kill();
    else if (command == "run") {
      std::string backend = backendSwitch.getValue();
      if ((success = proj.run(backendSwitch.getValue(), extras, verboseSwitch.getValue())))
        std::cout << "Job number: " << proj.property_get("jobnumber") << std::endl;
      else {
        std::cerr << "Run failed to start, or job number could not be captured" << std::endl
                  << "Status: " << status_message[proj.status(verboseSwitch.getValue())] << std::endl;
      }
    } else if (command == "sync") {
      bool more = !allArg.getValue() or molpro::project::recent(1) != "";
      for (int i = 1; more; ++i) {
        molpro::project proj(allArg.getValue() ? molpro::project::recent(i) : project);
        if (verboseSwitch.getValue()>0) std::cerr <<"Synchronize project "<< proj.filename()<<std::endl;
        auto cbe = proj.property_get("backend");
        if (cbe.empty()) {
          if (!allArg.getValue() and extras.empty())
            throw TCLAP::CmdLineParseException("Missing host");
          if (!extras.empty())
            success = proj.synchronize(extras.front(), verboseSwitch.getValue());
        } else {
          if (!extras.empty())
            throw TCLAP::CmdLineParseException(
                "Cannot synchronize with backend " + extras.front() + " because backend " + cbe + " is active");
          success = proj.synchronize(molpro::global_backend(cbe), verboseSwitch.getValue());
        }
        more = allArg.getValue() and molpro::project::recent(i + 1) != "";
      }
    } else if (command == "edit")
      success = system(("eval ${VISUAL:-${EDITOR:-vi}} \\'" + proj.filename("inp")+"\\'").c_str());
    else if (command == "browse") {
      if (!proj.property_get("backend").empty())
        success = proj.synchronize(molpro::global_backend(proj.property_get("backend")),
                                   verboseSwitch.getValue());
      if (success) success = system(("eval ${PAGER:-${EDITOR:-less}} \\'" + proj.filename("out")+"\\'").c_str());
    } else if (command == "clean") {
      proj.clean(true,allArg.getValue(),allArg.getValue());
    } else
      throw TCLAP::CmdLineParseException("Unknown subcommand: "+command);
    return success ? 0 : 1;

  } catch (TCLAP::ArgException &e)  // catch any exceptions
  { std::cerr << "error: " << e.error() << " for arg " << e.argId() << std::endl; }
  return 0;
}